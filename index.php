<body>
    <h3>Output Akhir</h3>
<?php
    
    require_once("animal.php");
    require_once("Frog.php");
    require_once("Ape.php");
    $sheep = new Animal("shaun");
    echo "Name: ".$sheep->name. "<br>"; // "shaun"
    echo "Legs: ".$sheep->legs. "<br>"; // 4
    echo "cold blooded: ". $sheep->cold_blooded. "<br><br>"; // "no"

    $kodok = new Frog("buduk");
    echo "Name: ".$kodok->name. "<br>"; // "shaun"
    echo "Legs: ".$kodok->legs. "<br>"; // 4
    echo "cold blooded: ". $kodok->cold_blooded. "<br>"; // "no"
    echo $kodok->jump(). "<br><br>"; // "hop hop"

    $sungokong = new Ape("kera sakti");
    echo "Name: ".$sungokong->name. "<br>"; // "shaun"
    echo "Legs: ".$sungokong->legs. "<br>"; // 4
    echo "cold blooded: ". $sungokong->cold_blooded. "<br>"; // "no"
    $sungokong->yell() // "Auooo"
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>
</body>